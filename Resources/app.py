import numpy as np

import sqlalchemy
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy import create_engine, func
import pandas as pd
from flask import Flask, jsonify

#################################################
# Database Setup
#################################################
engine = create_engine("sqlite:///hawaii.sqlite",
    connect_args={'check_same_thread': False})

# reflect an existing database into a new model
Base = automap_base()
# reflect the tables
Base.prepare(engine, reflect=True)

# Save reference to the table
Measurement = Base.classes.measurement
Station = Base.classes.station

# Create our session (link) from Python to the DB
session = Session(engine)

#################################################
# Flask Setup
#################################################
app = Flask(__name__)


#################################################
# Flask Routes
#################################################
@app.route("/")
def welcome():
    """List all available api routes."""
    return (
            f"Available Routes:<br/>"
            f"<a href='/api/v1.0/precipitation'>Precipitation</a><br/>"
            f"<a href='/api/v1.0/stations'>Stations</a><br/>"
            f"<a href='/api/v1.0/tobs'>Temperature Observations (tobs)</a><br/>"
        
            )

@app.route("/api/v1.0/precipitation")
def precipitation():
    
    # Convert the query results to a Dictionary using date as the key and prcp as the value.
    results = session.query(Measurement.date, Measurement.prcp).all()
    
    # Convert 
    all_prcp = list(np.ravel(results))
    
    return jsonify(all_prcp)

@app.route("/api/v1.0/stations")
def station_count():
    
    # Return a JSON list of stations from the dataset. 
    results = session.query(Station.station, Station.name).all()
    
    # Convert 
    stations = list(np.ravel(results))
  
    return jsonify(stations)

@app.route("/api/v1.0/tobs")
def tobs():
    
    # Query 
    results = session.query(Measurement.date, Measurement.tobs).all()

    all_tob = list(np.ravel(results))
    return jsonify(all_tob)



if __name__ == '__main__':
    app.run(debug=True)